ERPAL DHL csv export
http://drupal.org/project/erpal_dhl_csv

Provides functionality to create a csv file with customer data on invoice creation.
This csv file can be imported to paket.de to easily create parcel stickers.

* ================
* Installation
* ================

