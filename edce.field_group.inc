<?php
/**
 * @file
 * edce.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function edce_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_invoice_dhl|node|erpal_invoice|form';
  $field_group->group_name = 'group_invoice_dhl';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'erpal_invoice';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'DHL',
    'weight' => '10',
    'children' => array(
      0 => 'field_invoice_dhl_csv',
      1 => 'field_invoice_dhl_product',
      2 => 'field_invoice_coupon',
      3 => 'field_invoice_use_coupon',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_invoice_dhl|node|erpal_invoice|form'] = $field_group;

  return $export;
}
