<?php
/**
 * @file
 * Provides actions that can manipulate dhl coupons.
 */

/**
 * Callback for action delete coupons
 */
function dhl_coupon_delete_action(&$coupons, $context) {
  dhl_coupon_delete_multiple(array_keys($coupons));
}

/**
 * Callback for action use coupons
 */
function dhl_coupon_use_action(&$coupons, $context) {
  foreach ($coupons as $coupon) {
    $coupon->used = 1;
    $coupon->date_used = time();
    dhl_coupon_save($coupon);
  }
}

/**
 * Callback for action unuse coupons
 */
function dhl_coupon_unuse_action(&$coupons, $context) {
  foreach ($coupons as $coupon) {
    $coupon->used = 0;
    $coupon->date_used = 0;
    dhl_coupon_save($coupon);
  }
}
