<?php
/**
 * @file
 * Provides functionality to administrate dhl coupons.
 */

/**
 * Generates the coupon type editing form.
 */
function dhl_coupon_type_form($form, &$form_state, $coupon_type, $op = 'edit') {

  if ($op == 'clone') {
    $coupon_type->label .= ' (cloned)';
    $coupon_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $coupon_type->label,
    '#description' => t('The human-readable name of this coupon type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($coupon_type->type) ? $coupon_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $coupon_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'dhl_coupon_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this coupon type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($coupon_type->description) ? $coupon_type->description : '',
    '#description' => t('Description about the coupon type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save coupon type'),
    '#weight' => 40,
  );

  if (!$coupon_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete coupon type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('dhl_coupon_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing coupon_type.
 */
function dhl_coupon_type_form_submit(&$form, &$form_state) {
  $coupon_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  dhl_coupon_type_save($coupon_type);

  // Redirect user back to list of coupon types.
  $form_state['redirect'] = 'admin/structure/coupon-types';
}

function dhl_coupon_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/coupon-types/' . $form_state['dhl_coupon_type']->type . '/delete';
}

/**
 * coupon type delete form.
 */
function dhl_coupon_type_form_delete_confirm($form, &$form_state, $coupon_type) {
  $form_state['coupon_type'] = $coupon_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['coupon_type_id'] = array('#type' => 'value', '#value' => entity_id('dhl_coupon_type', $coupon_type));
  return confirm_form($form,
    t('Are you sure you want to delete coupon type %title?', array('%title' => entity_label('coupon_type', $coupon_type))),
    'coupon/' . entity_id('dhl_coupon_type', $coupon_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * coupon type delete form submit handler.
 */
function dhl_coupon_type_form_delete_confirm_submit($form, &$form_state) {
  $coupon_type = $form_state['coupon_type'];
  dhl_coupon_type_delete($coupon_type);

  watchdog('dhl_coupon_type', '@type: deleted %title.', array(
    '@type' => $coupon_type->type,
    '%title' => $coupon_type->label
  ));
  drupal_set_message(t('@type %title has been deleted.', array(
    '@type' => $coupon_type->type,
    '%title' => $coupon_type->label
  )));

  $form_state['redirect'] = 'admin/structure/coupon-types';
}

/**
 * Add new coupon page callback.
 */
function dhl_coupon_add() {

  $coupon = entity_create('dhl_coupon', array('type' => 'dhl_coupon'));
  drupal_set_title(t('Create DHL coupon'));

  $output = drupal_get_form('dhl_coupon_form', $coupon);

  return $output;
}

/**
 * Import coupons page callback.
 */
function dhl_coupon_import() {

  drupal_set_title(t('Import DHL coupons'));

  $output = drupal_get_form('dhl_coupon_import_form');
  return $output;
}

/**
 * coupon import Form.
 */
function dhl_coupon_import_form($form, &$form_state) {

  $form['file'] = array(
//    '#name' => 'dhl_coupon_file',
    '#type' => 'file',
    '#title' => t('Choose a DHL coupon set txt file'),
//    '#required' => TRUE,
  );

  $form['product'] = array(
    '#title' => t('Product'),
    '#default_value' => '',
    '#description' => t('The Product these coupons can be used for.'),
    '#required' => TRUE,
    "#type" => "select",
    "#options" => array(
      "Germany" => array(
        "PAECK.DEU" => t("Päckchen"),
        "PAK02.DEU" => t("Paket 2kg"),
        "PAK10.DEU" => t("Paket 10kg"),
        "PAK20.DEU" => t("Paket 20kg"),
        "PAK31.DEU" => t("Paket 31,5kg"),
      ),
      "Europe" => array(
        "PAECK.EU" => t("Päckchen"),
        "PAK02.EU" => t("Paket 2kg"),
        "PAK10.EU" => t("Paket 10kg"),
        "PAK20.EU" => t("Paket 20kg"),
      ),
    ),
  );

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import coupons'),
    '#submit' => $submit + array('dhl_coupon_imort_form_submit'),
  );

  $form['#validate'][] = 'dhl_coupon_import_form_validate';

  return $form;
}

/**
 * coupon Form.
 */
function dhl_coupon_form($form, &$form_state, $coupon) {
  $form_state['coupon'] = $coupon;

  $form['code'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Couponcode'),
    '#default_value' => $coupon->code,
  );

  $form['used'] = array(
    '#type' => 'checkbox',
    '#required' => FALSE,
    '#title' => t('Used'),
    '#default_value' => $coupon->used,
  );


  $format = erpal_lib_get_date_format(FALSE);
  $form['date_used'] = array(
    '#type' => 'date_popup',
    '#date_format' => $format,
    '#title' => t('Date Used'),
    '#attributes' => array('autocomplete' => 'off'),
    '#default_value' => $coupon->date_used ? date('Y-m-d', $coupon->date_used) : '',
    '#required' => FALSE,
    '#description' => 'Date when the coupon was used.',
    '#states' => array(
      // Hide the settings when the cancel notify checkbox is disabled.
      'invisible' => array(
        ':input[name="used"]' => array('checked' => FALSE),
      ),
    )
  );

  $form['product'] = array(
    '#title' => t('Product'),
    '#default_value' => $coupon->product,
    '#description' => t('The Product this coupon can be used for.'),
    '#required' => TRUE,
    "#type" => "select",
    "#options" => array(
      "Germany" => array(
        "PAECK.DEU" => t("Päckchen"),
        "PAK02.DEU" => t("Paket 2kg"),
        "PAK10.DEU" => t("Paket 10kg"),
        "PAK20.DEU" => t("Paket 20kg"),
        "PAK31.DEU" => t("Paket 31,5kg"),
      ),
      "Europe" => array(
        "PAECK.EU" => t("Päckchen"),
        "PAK02.EU" => t("Paket 2kg"),
        "PAK10.EU" => t("Paket 10kg"),
        "PAK20.EU" => t("Paket 20kg"),
      ),
    ),
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $coupon->uid,
  );

  field_attach_form('dhl_coupon', $coupon, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save coupon'),
    '#submit' => $submit + array('dhl_coupon_form_submit'),
  );

  // Show Delete button if we edit coupon.
  $coupon_id = entity_id('dhl_coupon', $coupon);
  if (!empty($coupon_id) && dhl_coupon_access('edit', $coupon)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('dhl_coupon_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'dhl_coupon_form_validate';

  return $form;
}

function dhl_coupon_form_validate($form, &$form_state) {

}

/**
 * coupon submit handler.
 */
function dhl_coupon_form_submit($form, &$form_state) {
  $coupon = $form_state['coupon'];

  entity_form_submit_build_entity('dhl_coupon', $coupon, $form, $form_state);

  //to go from 2013-09-29 22:22 to timestamp
  $coupon->date_used = $coupon->used ? strtotime($coupon->date_used) : 0;

  dhl_coupon_save($coupon);

  $coupon_uri = entity_uri('dhl_coupon', $coupon);

  $form_state['redirect'] = $coupon_uri['path'];

  drupal_set_message(t('coupon %title saved.', array('%title' => entity_label('dhl_coupon', $coupon))));
}

/**
 * coupon import form validation.
 */
function dhl_coupon_import_form_validate($form, &$form_state) {
  $file = file_save_upload('file', array(
    'file_validate_extensions' => array('txt'), // Validate extensions.
  ));

  // If the file passed validation:
  if (!$file) {
    form_set_error('file', t('No file was uploaded.'));
  }

}

/**
 * coupon import submit handler.
 */
function dhl_coupon_imort_form_submit($form, &$form_state) {
  $file = file_save_upload('file', array(
    'file_validate_extensions' => array('txt'), // Validate extensions.
  ));

  $re1 = '.*?'; # Non-greedy match on filler
  $re2 = '((?:[a-z][a-z]*[0-9]+[a-z0-9]*))'; # Alphanum 1
  $re3 = '(\\s+)'; # White Space 1

  if ($c = preg_match_all("/" . $re1 . $re2 . $re3 . "/is", file_get_contents($file->uri), $matches)) {
    $product = $form['product']['#value'];
    foreach ($matches[1] as $code) {
      $coupon = entity_create('dhl_coupon', array('type' => 'dhl_coupon', 'code' => $code, 'product' => $product));
      dhl_coupon_save($coupon);
    }
  }

  $form_state['redirect'] = 'dhl_coupon';

  drupal_set_message(t('Coupons imported.'));
}

function dhl_coupon_form_submit_delete($form, &$form_state) {
  $coupon = $form_state['coupon'];
  $coupon_uri = entity_uri('dhl_coupon', $coupon);
  $form_state['redirect'] = $coupon_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function dhl_coupon_delete_form($form, &$form_state, $coupon) {
  $form_state['coupon'] = $coupon;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['coupon_type_id'] = array('#type' => 'value', '#value' => entity_id('dhl_coupon', $coupon));
  $coupon_uri = entity_uri('dhl_coupon', $coupon);
  return confirm_form($form,
    t('Are you sure you want to delete coupon %title?', array('%title' => entity_label('dhl_coupon', $coupon))),
    $coupon_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function dhl_coupon_delete_form_submit($form, &$form_state) {
  $coupon = $form_state['coupon'];
  dhl_coupon_delete($coupon);

  drupal_set_message(t('coupon %title deleted.', array('%title' => entity_label('dhl_coupon', $coupon))));

  $form_state['redirect'] = 'dhl_coupon';
}
