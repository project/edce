<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on dhl_coupon being loaded from the database.
 *
 * This hook is invoked during $dhl_coupon loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $dhl_coupon entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_dhl_coupon_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $dhl_coupon is inserted.
 *
 * This hook is invoked after the $dhl_coupon is inserted into the database.
 *
 * @param DHLCoupon $dhl_coupon
 *   The $dhl_coupon that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_dhl_coupon_insert(DHLCoupon $dhl_coupon) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('dhl_coupon', $dhl_coupon),
      'extra' => print_r($dhl_coupon, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $dhl_coupon being inserted or updated.
 *
 * This hook is invoked before the $dhl_coupon is saved to the database.
 *
 * @param DHLCoupon $dhl_coupon
 *   The $dhl_coupon that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_dhl_coupon_presave(DHLCoupon $dhl_coupon) {
  $dhl_coupon->name = 'foo';
}

/**
 * Responds to a $dhl_coupon being updated.
 *
 * This hook is invoked after the $dhl_coupon has been updated in the database.
 *
 * @param DHLoupon $dhl_coupon
 *   The $dhl_coupon that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_dhl_coupon_update(DHLCoupon $dhl_coupon) {
  db_update('mytable')
    ->fields(array('extra' => print_r($dhl_coupon, TRUE)))
    ->condition('id', entity_id('dhl_coupon', $dhl_coupon))
    ->execute();
}

/**
 * Responds to $dhl_coupon deletion.
 *
 * This hook is invoked after the $dhl_coupon has been removed from the database.
 *
 * @param DHLCoupon $dhl_coupon
 *   The $dhl_coupon that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_dhl_coupon_delete(DHLCoupon $dhl_coupon) {
  db_delete('mytable')
    ->condition('pid', entity_id('dhl_coupon', $dhl_coupon))
    ->execute();
}

/**
 * Act on a dhl_coupon that is being assembled before rendering.
 *
 * @param $dhl_coupon
 *   The dhl_coupon entity.
 * @param $view_mode
 *   The view mode the dhl_coupon is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $dhl_coupon->content prior to rendering. The
 * structure of $dhl_coupon->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_dhl_coupon_view($dhl_coupon, $view_mode, $langcode) {
  $dhl_coupon->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for dhl_coupons.
 *
 * @param $build
 *   A renderable array representing the dhl_coupon content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * dhl_coupon content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the dhl_coupon rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_dhl_coupon().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_dhl_coupon_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

