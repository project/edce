<?php
/**
 * @file
 * Provides dhl coupon view callback.
 */

/**
 * Coupon view callback.
 */
function dhl_coupon_view($coupon) {
  drupal_set_title(entity_label('dhl_coupon', $coupon));
  return entity_view('dhl_coupon', array(entity_id('dhl_coupon', $coupon) => $coupon), 'full');
}
