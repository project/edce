<?php
/**
 * @file
 * Provides the views for dhl coupons.
 */

/**
 * Implements hook_views_default_views().
 */
function dhl_coupon_views_default_views() {
  $view = new view();
  $view->name = 'dhl_coupons';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'dhl_coupon';
  $view->human_name = 'Dhl Coupons';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Coupons';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'views_bulk_operations' => 'views_bulk_operations',
    'cpid' => 'cpid',
    'type' => 'type',
    'title' => 'title',
    'created' => 'created',
    'used' => 'used',
    'date_used' => 'date_used',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'cpid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'used' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'date_used' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<a href="/dhl_coupon/add">Add Coupon</a> <a href="/dhl_coupon/import">Import Coupons</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Field: Bulk operations: Coupon */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::erpal_project_release_add_tasks_to_release' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::erpal_invoice_helper_invoice_from_billables_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::billable_delete_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::dhl_coupon_delete_action' => array(
      'selected' => 1,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::erpal_projects_helper_timetracking_finalise_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::billable_reduce_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::erpal_project_release_remove_tasks_from_release' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::billable_billed_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::billable_unbilled_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::dhl_coupon_unuse_action' => array(
      'selected' => 1,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::dhl_coupon_use_action' => array(
      'selected' => 1,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Coupon: Coupon ID */
  $handler->display->display_options['fields']['cpid']['id'] = 'cpid';
  $handler->display->display_options['fields']['cpid']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['cpid']['field'] = 'cpid';
  /* Field: Coupon: Code */
  $handler->display->display_options['fields']['title']['id'] = 'code';
  $handler->display->display_options['fields']['title']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['title']['field'] = 'code';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'dhl_coupon/[cpid]';
  /* Field: Coupon: Product */
  $handler->display->display_options['fields']['product']['id'] = 'product';
  $handler->display->display_options['fields']['product']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['product']['field'] = 'product';
  /* Field: Coupon: Date created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created';
  $handler->display->display_options['fields']['created']['date_format'] = 'erpal_date';
  /* Field: Coupon: Date used */
  $handler->display->display_options['fields']['date_used']['id'] = 'date_used';
  $handler->display->display_options['fields']['date_used']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['date_used']['field'] = 'date_used';
  $handler->display->display_options['fields']['date_used']['empty'] = 'not used';
  $handler->display->display_options['fields']['date_used']['date_format'] = 'erpal_date';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'dhl_coupon/[cpid]/edit';
  /* Filter criterion: Coupon: Used */
  $handler->display->display_options['filters']['used']['id'] = 'used';
  $handler->display->display_options['filters']['used']['table'] = 'dhl_coupon';
  $handler->display->display_options['filters']['used']['field'] = 'used';
  $handler->display->display_options['filters']['used']['exposed'] = TRUE;
  $handler->display->display_options['filters']['used']['expose']['operator_id'] = 'used_op';
  $handler->display->display_options['filters']['used']['expose']['label'] = 'Used';
  $handler->display->display_options['filters']['used']['expose']['operator'] = 'used_op';
  $handler->display->display_options['filters']['used']['expose']['identifier'] = 'used';
  $handler->display->display_options['filters']['used']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['used']['group_info']['label'] = 'Used';
  $handler->display->display_options['filters']['used']['group_info']['identifier'] = 'used';
  $handler->display->display_options['filters']['used']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'true',
      'operator' => '=',
      'value' => array(
        'value' => '1',
        'min' => '',
        'max' => '',
      ),
    ),
    2 => array(
      'title' => 'false',
      'operator' => '=',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
    ),
  );
  /* Filter criterion: Coupon: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'dhl_coupon';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
  );
  $handler->display->display_options['filters']['type']['group_info']['label'] = 'Type';
  $handler->display->display_options['filters']['type']['group_info']['identifier'] = 'type';
  $handler->display->display_options['filters']['type']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['type']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Coupon: Used */
  $handler->display->display_options['filters']['used']['id'] = 'used';
  $handler->display->display_options['filters']['used']['table'] = 'dhl_coupon';
  $handler->display->display_options['filters']['used']['field'] = 'used';
  $handler->display->display_options['filters']['used']['exposed'] = TRUE;
  $handler->display->display_options['filters']['used']['expose']['operator_id'] = 'used_op';
  $handler->display->display_options['filters']['used']['expose']['label'] = 'Used';
  $handler->display->display_options['filters']['used']['expose']['operator'] = 'used_op';
  $handler->display->display_options['filters']['used']['expose']['identifier'] = 'used';
  $handler->display->display_options['filters']['used']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['used']['group_info']['label'] = 'Used';
  $handler->display->display_options['filters']['used']['group_info']['identifier'] = 'used';
  $handler->display->display_options['filters']['used']['group_info']['default_group'] = '2';
  $handler->display->display_options['filters']['used']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'true',
      'operator' => '=',
      'value' => array(
        'value' => '1',
        'min' => '',
        'max' => '',
      ),
    ),
    2 => array(
      'title' => 'false',
      'operator' => '=',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
    ),
  );
  /* Filter criterion: Coupon: Product */
  $handler->display->display_options['filters']['product']['id'] = 'product';
  $handler->display->display_options['filters']['product']['table'] = 'dhl_coupon';
  $handler->display->display_options['filters']['product']['field'] = 'product';
  $handler->display->display_options['filters']['product']['exposed'] = TRUE;
  $handler->display->display_options['filters']['product']['expose']['operator_id'] = 'product_op';
  $handler->display->display_options['filters']['product']['expose']['label'] = 'Product';
  $handler->display->display_options['filters']['product']['expose']['operator'] = 'product_op';
  $handler->display->display_options['filters']['product']['expose']['identifier'] = 'product';
  $handler->display->display_options['filters']['product']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['product']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['product']['group_info']['label'] = 'Product';
  $handler->display->display_options['filters']['product']['group_info']['identifier'] = 'product';
  $handler->display->display_options['filters']['product']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'Päckchen DEU',
      'operator' => '=',
      'value' => 'PAECK.DEU',
    ),
    2 => array(
      'title' => 'Paket 2Kg DEU',
      'operator' => '=',
      'value' => 'PAK02.DEU',
    ),
    3 => array(
      'title' => 'Paket 10Kg DEU',
      'operator' => '=',
      'value' => 'PAK10.DEU',
    ),
    4 => array(
      'title' => 'Paket 20Kg DEU',
      'operator' => '=',
      'value' => 'PAK20.DEU',
    ),
    5 => array(
      'title' => 'Paket 31,5Kg DEU',
      'operator' => '=',
      'value' => 'PAK31.DEU',
    ),
    6 => array(
      'title' => 'Päckchen EU',
      'operator' => '=',
      'value' => 'PAECK.EU',
    ),
    7 => array(
      'title' => 'Paket 2Kg EU',
      'operator' => '=',
      'value' => 'PAK02.EU',
    ),
    8 => array(
      'title' => 'Paket 10Kg EU',
      'operator' => '=',
      'value' => 'PAK10.EU',
    ),
    9 => array(
      'title' => 'Paket 20Kg EU',
      'operator' => '=',
      'value' => 'PAK20.EU',
    ),
    10 => array(
      'title' => '',
      'operator' => '=',
      'value' => '',
    ),
  );
  $handler->display->display_options['path'] = 'dhl_coupon';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Coupons';
  $handler->display->display_options['menu']['weight'] = '0';

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'title' => 'title',
    'product' => 'product',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Coupon: Code */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['title']['field'] = 'code';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['path'] = 'dhl_coupon/[cpid]';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Coupon: Product */
  $handler->display->display_options['fields']['product']['id'] = 'product';
  $handler->display->display_options['fields']['product']['table'] = 'dhl_coupon';
  $handler->display->display_options['fields']['product']['field'] = 'product';
  $handler->display->display_options['fields']['product']['label'] = '';
  $handler->display->display_options['fields']['product']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Coupon: Used */
  $handler->display->display_options['filters']['used']['id'] = 'used';
  $handler->display->display_options['filters']['used']['table'] = 'dhl_coupon';
  $handler->display->display_options['filters']['used']['field'] = 'used';
  $handler->display->display_options['filters']['used']['value']['value'] = '0';
  $handler->display->display_options['filters']['used']['expose']['operator_id'] = 'used_op';
  $handler->display->display_options['filters']['used']['expose']['label'] = 'Used';
  $handler->display->display_options['filters']['used']['expose']['operator'] = 'used_op';
  $handler->display->display_options['filters']['used']['expose']['identifier'] = 'used';
  $handler->display->display_options['filters']['used']['is_grouped'] = TRUE;
  $handler->display->display_options['filters']['used']['group_info']['label'] = 'Used';
  $handler->display->display_options['filters']['used']['group_info']['identifier'] = 'used';
  $handler->display->display_options['filters']['used']['group_info']['group_items'] = array(
    1 => array(
      'title' => 'true',
      'operator' => '=',
      'value' => array(
        'value' => '1',
        'min' => '',
        'max' => '',
      ),
    ),
    2 => array(
      'title' => 'false',
      'operator' => '=',
      'value' => array(
        'value' => '0',
        'min' => '',
        'max' => '',
      ),
    ),
    3 => array(
      'title' => '',
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
    ),
  );


  $views['coupons'] = $view;

  return $views;
}