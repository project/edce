<?php
/**
 * @file
 * Provides dhl coupon entity and its controller.
 */

class DHLCouponController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'code' => '',
      'used' => 0,
      'date_used' => 0,
      'product' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $content['used'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Used'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'boolean',
      '#entity_type' => 'dhl_coupon',
      '#bundle' => $entity->type,
      '#items' => array(array('value' => $entity->used)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $entity->used ? t('yes') : t('no'))
    );
    if ($entity->used) {
      $content['date_used'] = array(
        '#theme' => 'field',
        '#weight' => 0,
        '#title' => t('Date Used'),
        '#access' => TRUE,
        '#label_display' => 'above',
        '#view_mode' => 'full',
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'field_fake_description',
        '#field_type' => 'date',
        '#entity_type' => 'dhl_coupon',
        '#bundle' => $entity->type,
        '#items' => array(array('value' => $entity->date_used)),
        '#formatter' => 'date',
        0 => array('#markup' => $entity->date_used ? date(erpal_lib_get_date_format(FALSE), $entity->date_used) : '')
      );
    }
    $content['product'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Product'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'select',
      '#entity_type' => 'dhl_coupon',
      '#bundle' => $entity->type,
      '#items' => array(array('value' => $entity->product)),
      '#formatter' => 'text_default',
      0 => array('#markup' => $entity->product),
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

/**
 * Coupon class.
 */
class DHLCoupon extends Entity {
  protected function defaultLabel() {
    return $this->code;
  }

  protected function defaultUri() {
    return array('path' => 'dhl_coupon/' . $this->identifier());
  }
}

