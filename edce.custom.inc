<?php
/**
 * @file
 * edce.custom.inc
 */

/**
 * Implements hook_node_update
 */
function edce_node_update($node) {

  //it must be an invoice node and dhl csv must be selected
  $type = node_type_get_type($node);
  $dhl_csv = field_get_items('node', $node, 'field_invoice_dhl_csv');

  if ($type->type == 'erpal_invoice' && $dhl_csv[0]['value']) {
    _edce_add_dhl_csv_on_save($node);
  }
}


/**
 * Attach csv file on invoice node save
 */
function _edce_add_dhl_csv_on_save($invoice_node) {

  //create an csv file only if the contractor is the own company
  $my_company_nid = _erpal_basic_helper_get_own_company_nid(FALSE);
  $contractor_nid = isset($invoice_node->field_contractor_ref[LANGUAGE_NONE][0]['target_id']) ? $invoice_node->field_contractor_ref[LANGUAGE_NONE][0]['target_id'] : FALSE;

  if ($my_company_nid == $contractor_nid) {
    $csv = _erpal_dhl_csv_create_file($invoice_node);
    if (!$csv) {
      drupal_set_message(t('The CSV could not be created.'), 'error');
    }
    else {
      //now only save the fields, not the node itself
      field_attach_update('node', $invoice_node);
    }
  }
}

/**
 * creates a csv file from an invoice
 * @param $invoice_node
 * @return bool
 */
function _erpal_dhl_csv_create_file($invoice) {
  global $user;

  field_attach_update('node', $invoice);

  $invoice_number = $invoice->field_invoice_number[LANGUAGE_NONE][0]['value'];
  $basename = _erpal_docs_make_filename(t('Invoice') . "-" . $invoice_number . '-dhl', 'csv');

  $doc_uri = _erpal_docs_folder_uri();
  $destination_path = drupal_realpath($doc_uri . $basename);
  //perhaps file must be renamed
  $destination_path = file_destination($destination_path, FILE_EXISTS_RENAME);
  $new_basename = basename($destination_path);
  $doc_uri = $doc_uri . $new_basename;
  //sometimes, the URI starts with /// instead of // so we fix that here
  $doc_uri = str_replace('///', '//', $doc_uri);

  $list = _edce_create_csv($invoice);

  //open the file and insert all the lines of the array
  $fp = fopen($destination_path, 'w');
  foreach ($list as $fields) {
    fputcsv($fp, $fields);
  }
  fclose($fp);

  // Begin building file object.
  $file = new stdClass();
  $file->uid = $user->uid;
  $file->status = 1; //final!
  $file->filename = $basename;
  $file->uri = $doc_uri;
  $file->filemime = file_get_mimetype($destination_path);
  $file->filesize = filesize($destination_path);

  file_save($file);

  //if file node, don't create a new attachment
  $file_node = _erpal_invoice_helper_invoice_file_node($invoice, $basename);
  _erpal_docs_attach_file($invoice, $file, $file_node);
  return TRUE;
}


/**
 * Create an array, that contains all the data.
 * @param $invoice invoice node
 * @return array with csv data
 */
function _edce_create_csv($invoice) {
  $product = field_get_items('node', $invoice, 'field_invoice_dhl_product');
  $contractor_id = field_get_items('node', $invoice, 'field_contractor_ref');
  $customer_id = field_get_items('node', $invoice, 'field_customer_ref');
  $contractor = node_load($contractor_id[0]['target_id']);
  $customer = node_load($customer_id[0]['target_id']);

  //if shipping address is selected, use it, otherwise use invoice address
  $shipping_address_id = field_get_items('node', $invoice, 'field_shipping_address_id');
  if (empty($shipping_address_id)) {
    $invoice_address_id = field_get_items('node', $invoice, 'field_invoice_address_id');
    $customer_address = entity_load_single('field_collection_item', $invoice_address_id[0]['value']);
  }
  else {
    $customer_address = entity_load_single('field_collection_item', $shipping_address_id[0]['value']);
  }

  $address_entity = _erpal_basic_helper_get_my_address_entity();

  return array(
    array(
      'SEND_NAME1',
      'SEND_NAME2',
      'SEND_STREET',
      'SEND_HOUSENUMBER',
      'SEND_PLZ',
      'SEND_CITY',
      'SEND_COUNTRY',
      'RECV_NAME1',
      'RECV_NAME2',
      'RECV_STREET',
      'RECV_HOUSENUMBER',
      'RECV_PLZ',
      'RECV_CITY',
      'RECV_COUNTRY',
      'PRODUCT',
      'COUPON'
    ),
    array(
      //SEND_NAME1
      isset($address_entity->field_address_name[LANGUAGE_NONE]) ? $address_entity->field_address_name[LANGUAGE_NONE][0]['value'] : $contractor->title,
      //SEND_NAME2
      isset($address_entity->field_addition_to_address[LANGUAGE_NONE]) ? $address_entity->field_addition_to_address[LANGUAGE_NONE][0]['value'] : '',
      //SEND_STREET
      isset($address_entity->field_street[LANGUAGE_NONE]) ? $address_entity->field_street[LANGUAGE_NONE][0]['value'] : '',
      //SEND_HOUSENUMBER
      '',
      //SEND_PLZ
      isset($address_entity->field_zip_code[LANGUAGE_NONE]) ? $address_entity->field_zip_code[LANGUAGE_NONE][0]['value'] : '',
      //SEND_CITY
      isset($address_entity->field_city[LANGUAGE_NONE]) ? $address_entity->field_city[LANGUAGE_NONE][0]['value'] : '',
      //SEND_COUNTRY TODO extract country code and convert to ISO3
      'DEU',
      //RECV_NAME1
      isset($customer_address->field_address_name[LANGUAGE_NONE]) ? $customer_address->field_address_name[LANGUAGE_NONE][0]['value'] : $customer->title,
      //RECV_NAME2
      isset($customer_address->field_addition_to_address[LANGUAGE_NONE]) ? $customer_address->field_addition_to_address[LANGUAGE_NONE][0]['value'] : '',
      //RECV_STREET
      isset($customer_address->field_street[LANGUAGE_NONE]) ? $customer_address->field_street[LANGUAGE_NONE][0]['value'] : '',
      //RECV_HOUSENUMBER
      '',
      //RECV_PLZ
      isset($customer_address->field_zip_code[LANGUAGE_NONE]) ? $customer_address->field_zip_code[LANGUAGE_NONE][0]['value'] : '',
      //RECV_CITY
      isset($customer_address->field_city[LANGUAGE_NONE]) ? $customer_address->field_city[LANGUAGE_NONE][0]['value'] : '',
      //RECV_COUNTRY TODO extract country code and convert to ISO3
      'DEU',
      //PRODUCT
      isset($product[0]) ? $product[0]['value'] : '',
      //COUPON
      $invoice->field_invoice_use_coupon[LANGUAGE_NONE][0]['value'] && isset($invoice->field_invoice_coupon[LANGUAGE_NONE]) ? dhl_coupon_load($invoice->field_invoice_coupon[LANGUAGE_NONE][0]['target_id'])->code : '',
    ),
  );

}